
import { useStore } from "../store"
import "./Task.css"
import trash from '../assets/trash.svg'
import classNames from "classnames"

export default function Task({title}){

    const task = useStore((store)=>store.tasks.find((task)=>task.title === title))
    
    const deleteTask = useStore((store)=>store.deleteTask)
    const setDraggedTask = useStore((store)=> store.setDraggedTask)

    return <div className="task" draggable
        onDragStart={()=>setDraggedTask(task.title)}>
        {task.title}
        <div className="bottomWrapper">
            <div>
                <img  src={trash} className="" onClick={()=>{deleteTask(task.title)}}/>
            </div>
            <div className={classNames('status', task.state)}>{task.state}</div>
        </div>
    </div>
} 