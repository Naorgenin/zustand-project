import { createWithEqualityFn  } from 'zustand/traditional'
// import Tracker from '@openreplay/tracker';
// import trackerZustand from '@openreplay/tracker-zustand';

// const tracker = new Tracker({
//     projectKey: "3BJicQKAu8W228LLl6wT",
//     ingestPoint: "https://localhost/ingest",
//     __DISABLE_SECURE_MODE: true,
//     respectDoNotTrack:false

    
// });
// const zustandPlugin = tracker.use(trackerZustand())
// const bearStoreLogger = zustandPlugin('<TestStore></TestStore>')

const store= (set) =>({
    tasks: [{title: 'Test', state : "ONGOING"}],
    draggedTask: null,


    addTask: (title,state)=>
    set(store=>({tasks: [...store.tasks,{title,state}]})),

    deleteTask: (title)=>
    set(store=>({tasks: store.tasks.filter((task)=> task.title !==title)})),

    setDraggedTask: (title) => {set( {draggedTask: title})},
    moveTask : (title,state) =>
    set((store)=> (
        {
        tasks: store.tasks.map((task)=>
        task.title === title ? {title,state}: task)
        })
    )
}) 

export const useStore = createWithEqualityFn(store)