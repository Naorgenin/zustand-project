import { record } from 'rrweb'
import rrwebPlayer from 'rrweb-player';
import 'rrweb-player/dist/style.css';
import './App.css'
import Column from './components/Column'
import { useEffect } from 'react';
// import data from './video.json'
let data= []
// import { H } from 'highlight.run';
function App() {
//####  openreplay
//  import Tracker from '@openreplay/tracker';
  // const tracker = new Tracker({
  //   projectKey: "3BJicQKAu8W228LLl6wT",
  //   ingestPoint: "https://localhost/ingest",
  //     __DISABLE_SECURE_MODE: true,
  //     respectDoNotTrack:false
  // });

  // tracker.start();

  // console.log(tracker)

  // ######

 // ### highlight io
//   H.init('1jdkoe52', {
//     serviceName: "frontend-app-test",
//     tracingOrigins: true,
//     networkRecording: {
//       enabled: true,
//       recordHeadersAndBody: true,
//       urlBlocklist: [],
//     },
//   });
// H.identify('naor', {
// 		id: 'very-secure-id',
// 		phone: '867-5309',
// 		bestFriend: 'naor'
// 	});
// ###### highlight io
let Events = [];

// let stopFn=record({
//     emit(event) {
//       Events.push(event);
//       if(Events.length > 5000){
//         stopFn()
//         console.log(Events.length)
//       }

//     }
// });
// DEBUG
// const show =  () =>{
//   console.log("events",Events)

// }

function AddtoReplay() { 
    console.log("Current events")
    console.log(Events)
    data=data.concat(Events)
    console.log("Total session")
    console.log(data)


    Events=[]
    // stopFn()

}
const playVideo = async () =>{
  console.log("Events")
  console.log(data)


new rrwebPlayer({
  target: document.body, 
  props: {
    width: 1000,
    height: 1280,
    events: data,
  },
});
}

useEffect(()=>{
record({
    emit(event) {

      // Events.push(event);
    Events.push(event)
// setInterval(save, 10 * 1000);
    },
    // sampling: {
    // // do not record mouse movement
    // mousemove: true,
    // // do not record mouse interaction
    // mouseInteraction: true,
    // // set the interval of scrolling event
    // scroll: 150 ,// do not emit twice in 150ms
    // // set the interval of media interaction event
    // media: 450
    // }
});
} )

  return (
    <div className='App'>
      <Column state="PLANNED"></Column>
      <Column state="ONGOING"></Column>
      <Column state="DONE"></Column>
      {/* <button onClick={()=>show()}> Show </button> */}
      <button onClick={()=>AddtoReplay()}> AddToreplay </button>
      <button onClick={()=>playVideo()}> Replay </button>
    </div>
  )
}

export default App
